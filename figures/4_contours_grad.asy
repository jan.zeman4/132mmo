settings.outformat="pdf";

size(200);

import contour;
import graph;

pair x_opt = (0.5,0.5);

real f( real x, real y ){

    pair z = (x,y);

    pair x_loc = rotate(-20)*( z - x_opt );

    return(2 + (0.8*x_loc.x^4 + 1.2*x_loc.y^4 ));
}

path grad(pair z) {
    pair x_loc = rotate(-20)*( z - x_opt );

    pair g_loc = ( 3.2*x_loc.x^3, 4.8*x_loc.y^3 );

    return( (0,0)--rotate(20)*g_loc );
}


// Data for contours
real[] c={2.01, 2.05, 2.1, 2.20, 2.3, 2.5, 3, 4, 5};

draw( contour(f, x_opt - (1,1), x_opt + (1,1), c), gray(.8) );
add( vectorfield(grad, x_opt - (1,1), x_opt + (1,1), 18, 18, true));

xaxis(-0.75, 1.75, gray(0.5), Arrow() );
yaxis(-0.75, 1.75, gray(0.5), Arrow() );

dot((x_opt.x, x_opt.y), red);
