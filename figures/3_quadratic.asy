settings.outformat="pdf";

import graph;

unitsize(1cm);

real x0 = 2;

include "3_functions";

draw((-5,0)--(5,0), Arrow(4) );
draw((0,-.5)--(0,tay2(-5)), Arrow(4));

draw(graph(tay2, -5, 5));
