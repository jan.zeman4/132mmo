settings.outformat="pdf";

import graph;

unitsize(1cm);

real x0 = 1.5;

include "3_functions";

draw((-3,0)--(5,0), Arrow(3) );
draw((x0,tay1(-3))--(x0,tay1(5)), Arrow(3));

draw(graph(tay0, -3, 5));
draw(graph(tay1, -3, 5));
