settings.outformat="pdf";

import graph;

unitsize(1cm);

real x0 = 2.25;

include "3_functions";

draw((-5,0)--(5,0), Arrow(4) );
draw((0,-.5)--(0,tay2(-5)), Arrow(4));

draw(graph(f,-5,5));
draw((xopt,f(xopt)), red);
draw((xopt,0)--(xopt,f(xopt)), red+gray);

// Initial surrogate
real x1;
draw((x0,0)--(x0,f(x0)), dotted+gray);
dot((x0,f(x0)));
draw(graph(tay2, -5, 5), dotted+gray);
x1 = x0 - f1(x0) / f2(x0); 
dot((x1,tay2(x1)), gray);
x0 = x1;

// 1st iteration
draw((x0,0)--(x0,f(x0)), dotted+gray);
dot((x0,f(x0)));
draw(graph(tay2, -5, 5), dotted+gray);
x1 = x0 - f1(x0) / f2(x0); 
dot((x1,tay2(x1)), gray);


// 2nd iteration
x0 = x1;
draw((x0,0)--(x0,f(x0)), dotted+gray);
dot((x0,f(x0)));
draw(graph(tay2, -5, 5), dotted+gray);
x1 = x0 - f1(x0) / f2(x0); 
dot((x1,tay2(x1)), gray);
