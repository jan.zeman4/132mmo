settings.outformat="png";
settings.render = 16;

size(150,0);

import graph3;

pair x_opt = (0.5,0.5);

real f( pair z ){

    pair x_loc = rotate(-20)*( z - x_opt );

    return(2 + (0.8*x_loc.x^2 + 1.2*x_loc.y^2 ));
}

surface s_f = surface( f, x_opt + (-1,-1), x_opt + (1,1), nx = 12, Spline );

currentprojection=perspective(3,5,8);

xaxis3( -0.5, 1.5, Arrow3() );
yaxis3( -0.5, 1.5, Arrow3() );
zaxis3( -0.25, 5, Arrow3() );

draw(s_f, surfacepen=material(white+opacity(0.8),
ambientpen=white), meshpen=gray(0.2));

dot((x_opt.x, x_opt.y, f( x_opt )), red);

draw( (x_opt.x, x_opt.y, 0)--(x_opt.x, x_opt.y, f(x_opt)), red);
draw( (x_opt.x, 0, 0) -- (x_opt.x, x_opt.y,0), red);
draw( (0, x_opt.y, 0) -- (x_opt.x, x_opt.y,0), red);