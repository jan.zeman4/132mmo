import three;
import graph3;

settings.outformat="pdf";
settings.render = 8;

size(10cm,0);

// Objective function data
real f1 = 51.49;
real f2 = 8.39;

// Nutrition value data
real k1 = 331.;
real k2 = 93.;
real K = 1000.;

// Scale z axis
real zscale = 0.05;

real a1 = K/k1;
real a2 = K/k2;

real zmax;

if(f1*a1 > f2*a2) 
{
  zmax = f1*a1;
}
else{
  zmax = f2*a2;
}

currentprojection=perspective(zscale3(zscale)*(10*a1,10*a2,10*.5*zmax));

real zmin = -.1 * zmax;

triple E = (a1, 0, zmin);
triple F = (0, a2, zmin);
triple H = (0, a2, zmax);
triple G = (a1, 0, zmax);

path3 constaint = E--F--H--G--cycle;

triple A = (a1, 0, a1*f1);
triple B = (zmin/f1, 0, zmin);
triple C = (0, zmin/f2, zmin);
triple D = (0, a2, a2*f2);

path3 objective_function = A--C--B--D--cycle;

draw(surface(zscale3(0.1)*constaint), white+opacity(0.1));
draw(surface(zscale3(0.1)*objective_function), gray+opacity(0.4));

axes3();