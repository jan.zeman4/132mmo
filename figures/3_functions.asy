real xopt = -1; // Optimum

real f( real x ){
    return ( exp(0.05*(x-xopt)**2) );
}

real f1(real x){
    return ( f(x)*0.1*(x-xopt) );
}

real f2(real x){
    return( 0.1*f(x) + f(x)*(0.1*(x-xopt))**2 );
}

real tay0(real x)
{
    return( f(x0) );
}

real tay1(real x)
{
    return( tay0(x) + f1(x0)*(x-x0) );
}

real tay2(real x){
    return( tay1(x) + .5*f2(x0)*(x-x0)**2 );
}