settings.outformat="png";
settings.render = 16;

size(150,0);

import graph3;

pair x_opt = (0.5,0.5);

real f( pair z ){

return( 3 - 2*z.x + z.y );
}

surface s_f = surface( f, x_opt + (-1,-1), x_opt + (1,1), nx = 12, Spline );

currentprojection=perspective(3,5,8);

xaxis3( -0.5, 1.5, Arrow3() );
yaxis3( -0.5, 1.5, Arrow3() );
zaxis3( -0.25, 5, Arrow3() );

draw(s_f, surfacepen=material(white+opacity(0.8),
ambientpen=white), meshpen=gray(0.2));
