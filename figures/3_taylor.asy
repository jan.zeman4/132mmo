settings.outformat="pdf";

import graph;

unitsize(1cm);

real x0 = 2;

include "3_functions";

draw((-5,0)--(5,0), Arrow(4) );
draw((0,tay1(-5))--(0,tay2(-5)), Arrow(4));

draw(graph(tay0, -5, 5), dashed+gray);
draw(graph(tay1, -5, 5), dashed+gray);
draw(graph(tay2, -5, 5), gray);

draw(graph(f,-5,5));
dot((x0,f(x0)));

real x1 = x0 - f1(x0) / f2(x0);

dot((x1,tay2(x1)), gray);
