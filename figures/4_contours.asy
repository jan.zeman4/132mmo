settings.outformat="pdf";

size(200);

import contour;
import graph;

pair x_opt = (0.5,0.5);

real f( real x, real y ){

    pair z = (x,y);

    pair x_loc = rotate(-20)*( z - x_opt );

    return(2 + (0.8*x_loc.x^4 + 1.2*x_loc.y^4 ));
}

// Data for contours
real[] c={2.01, 2.1, 2.3, 3, 5};

Label[] Labels=sequence(new Label(int i) {
    return Label( string( c[i], 3), Relative(unitrand()),(0,0),
                 UnFill(1bp) ); },c.length);


draw( Labels, contour(f, x_opt - (1,1), x_opt + (1,1), c), fontsize(7pt) );

xaxis(-0.75, 1.75, gray(0.5), Arrow() );
yaxis(-0.75, 1.75, gray(0.5), Arrow() );

dot((x_opt.x, x_opt.y), red);

draw( (x_opt.x, 0) -- (x_opt.x, x_opt.y), red + dotted);
draw( (0, x_opt.y) -- (x_opt.x, x_opt.y), red + dotted);
