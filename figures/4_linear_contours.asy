settings.outformat="pdf";

size(200);

import contour;
import graph;

pair x_opt = (0.5,0.5);

real f( real x, real y ){ return(3 - 2*x + y ); }

// Data for contours
real[] c={0, 1, 2, 3, 4, 5};

Label[] Labels=sequence(new Label(int i) {
    return Label( string( c[i], 3), Relative(unitrand()),(0,0),
                 UnFill(1bp) ); },c.length);


draw( Labels, contour(f, x_opt - (1,1), x_opt + (1,1), c), fontsize(7pt) );

xaxis(-0.75, 1.75, gray(0.5), Arrow() );
yaxis(-0.75, 1.75, gray(0.5), Arrow() );