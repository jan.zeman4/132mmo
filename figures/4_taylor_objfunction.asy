settings.outformat="png";
settings.render = 16;

size(150,0);

import graph3;

pair x_opt = (0.5,0.5);

pair x0 = (0,0); // Point of Taylor expansion

real f( pair z ){

    pair x_loc = rotate(-20)*( z - x_opt );

    return(2 + (0.8*x_loc.x^4 + 1.2*x_loc.y^4 ));
}

pair grad(pair z) {
    pair x_loc = rotate(-20)*( z - x_opt );

    pair g_loc = ( 3.2*x_loc.x^3, 4.8*x_loc.y^3 );

    return( rotate(20)*g_loc );
}

real f_taylor( pair z){
    real val = f(x0);

    // Gradient correction
    pair g = grad(x0);
    val = val + g.x*(z.x - x0.x) + g.y*(z.y - x0.y);    

    pair t = rotate(-20)*(1,0);

    real T11 = t.x;
    real T21 = t.y;

    t = rotate(-20)*(0,1);

    real T12 = t.x;
    real T22 = t.y;
    
    pair x0_loc = rotate(-20)*( x0 - x_opt );

    real g11 = 9.6*x0_loc.x^2;
    real g22 = 14.4*x0_loc.y^2;

    real H11 = g11*T11^2 + g22*T21^2;
    real H12 = g11*T11*T12 + g22*T21*T22;
    real H22 = g11*T12^2 + g22*T22^2;

    val = val + .5*H11*(z.x-x0.x)^2 + H12*(z.x - x0.x)*(z.y - x0.y)^2 + .5*H22*(z.y-x0.y)^2;

    return( val );
}


surface s_f = surface( f, x_opt + (-1,-1), x_opt + (1,1), nx = 12, Spline );
surface s_ft = surface( f_taylor, x_opt + (-1,-1), x_opt + (.6,.6), nx = 12, Spline );

currentprojection=perspective(3,5,8);

xaxis3( -0.5, 1.5, Arrow3() );
yaxis3( -0.5, 1.5, Arrow3() );
zaxis3( -0.25, 5, Arrow3() );

draw(s_f, surfacepen=material(white+opacity(0.5),
ambientpen=white), meshpen=white);

draw(s_ft, surfacepen=material(white+opacity(0.8),
ambientpen=white), meshpen=gray(0.2));


dot((x0.x, x0.y, f( x0 )), red);

zlimits(-.25,2);