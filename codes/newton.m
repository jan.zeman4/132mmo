function newton
    function val = f(x)
       val = (x(1,:)-2).^2 + (x(1,:)-2*x(2,:).^2).^2;
    end

    function val = g(x)
        val = zeros(2,1);
        val(1) = -4*x(2)^2 + 4*x(1) - 4;
        val(2) = 8*x(2)*(2*x(2)^2-x(1));
    end

    function val = H(x)
        val = zeros(2,2);
        val(1,1) = 4;
        val(1,2) = -8*x(2); 
        val(2,1) = val(1,2);
        val(2,2) = 8*(6*x(2)^2-x(1));
    end

    function val = fdir(alpha)
        val = f(xk+dk*alpha);
    end

xk = [2;2];
tolerance = 1e-6;
%%
clf;
h = ezcontour(@(x,y)f([x';y']), [-1,5,-3,3]);
h.LevelList = [0:0.5:10];
colorbar;
colormap gray;
hold on;
%%
k = 0;
gk = g(xk);

while(norm(gk) > tolerance)
    disp(['iteration = ' num2str(k) ' || gradient || = ' ... 
        num2str(norm(gk)) ' f = ' num2str(f(xk))]);
    plot(xk(1),xk(2),'or');
    pause;
%%        
    Hk = H(xk);
    dk = -Hk\gk;
%%    
    if(dk'*gk>=0 || max(isnan(dk))==1)
        disp('correcting');
        dk = -gk;
    end
%%    
    alphak = fminbnd(@fdir,0,10);
    disp(['linesearch: alpha = ' num2str(alphak)]);
%%    
    xk = xk + alphak*dk;
    gk = g(xk);
    k = k+1;
end
%%
disp(['Convergence: iteration = ' num2str(k) ...
    ' ||gradient|| = ' num2str(norm(gk))]);
xk
f(xk)
end