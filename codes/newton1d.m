function newton1d

    function val = f(x) 
        val = (x.^2-1).^2+x;
    end

    function val = g(x) 
        val = 4*(x^3-x)+1;
    end

    function val = h(x) 
        val = 4*(3*x^2-1);
    end

tolerance = 1e-6;
xk = -2;
%%
k = 0;

clf;
ezplot(@(x)f(x), [-2.25,2.25]);
hold on;
%%
gk = g(xk);

while(abs(gk) > tolerance)
    disp(['iteration = ' num2str(k) ' x = ' num2str(xk) ...
        ' gradient = ' num2str(gk) ]);
    plot(xk,f(xk),'or');
%%    
    hk = h(xk);
    dk = -gk/hk;
    xk = xk+dk;
    gk = g(xk);
    k = k+1;
    pause
end
end