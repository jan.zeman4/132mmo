V = load( 'shares_history.dat' ); 
[n,m] = size(V); 
p = 20;

plot(V,'*');
legend('KOMB','CEZ','UNIPE','TABAK');
xlabel('Year'); 
ylabel('Value');
%%
R=zeros(n-1,m); 
for i=1:n-1
    R(i,:)=(V(i+1,:)-V(i,:))./V(i,:)*100;
end
ER = mean(R);
%%
plot(R,'*');
legend('KOMB','CEZ','UNIPE','TABAK');
xlabel('Year');
ylabel('Annual return');
%%
C=cov(R);
%%
x=quadprog(C,zeros(m,1),-ER,-p,ones(1,m),1,zeros(m,1),[]);

x 
ER*x 
%%
Vreal=load('shares_real.dat');

Rreal=(Vreal-V(n,:))./V(n,:)*100;
Rreal*x 
