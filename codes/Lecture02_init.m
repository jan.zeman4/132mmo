folder = "132mmo"; % Target folder for the source files
repo = "https://gitlab.com/jan.zeman4/132mmo/-/raw/master/codes"; 

%% Delete/create the target folder
if( isfolder( folder ) )
    rmdir( folder, "s" );
end

mkdir( folder );

%% Download files
websave(strcat(folder, "/portfolio.mlx"), strcat(repo, "/portfolio.mlx"));
websave(strcat(folder, "/values_history.dat"), strcat(repo, "/values_history.dat"));
websave(strcat(folder, "/values_real.dat"), strcat(repo, "/values_real.dat"));
