\documentclass{beamer}

\mode<presentation>
{
\usetheme{boxes}
\usecolortheme{wolverine}
%\usefonttheme{structurebold}
\usefonttheme{professionalfonts}
\setbeamercovered{transparent}
}

\usepackage[english]{babel}
%\usepackage[T1]{fontenc}

\usepackage{mathptmx}

\title[MMO / Lecture 1]{Modern Methods of Optimization}
\subtitle{Lecture 1: General overview}

\date{Faculty of Civil Engineering / CTU in Prague}

\input{format.ltx}

\newcommand{\paragraph}[1]{}

\begin{document}

\begin{frame}
\titlepage

\EuroTeQackno

\end{frame}

\begin{frame}
\frametitle{Outline}
\tableofcontents
\end{frame}

\section{Motivation of optimization problems}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Motivation [Why?]}
\framesubtitle{How to eat as an economist?}

\begin{description}
  \item[Objective] choose the \emph{most economic} menu
  \item[Constraints] must meet \emph{daily nutrition requirements}
\end{description}

\begin{center}

\begin{tabular}{lcc}
\hline
Item & Price & Nutrition \\
& [CZK/1kg] & [Cal/100g] \\
\hline
Roast pork & 120.10 & 273 \\
Chicken    &  51.49 & 331\\
Potatoes   &   8.39 & 93 \\
Bread      &  15.56 & 260\\
$\ldots$ \\
\hline
\end{tabular}

\end{center}

\begin{itemize}
  \item Optimal diet problem~\cite{Dantzig:1990:DP}
  \item First studied in 40's in military applications
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Motivation [Why?]}
\framesubtitle{How to invest your money?}

\begin{description}
  \item[Objective] invest your money with the \emph{minimal risk}
  \item[Constraint] achieve at least the given \emph{profit}  
\end{description}

\begin{center}
\begin{tabular}{lrrrrrr}
\hline
Share & \multicolumn{5}{c}{\href{https://www.akcie.cz}{Price~[CZK]}} \\
      & 2001 & 2002 & 2003 & 2004 & \ldots \\
\hline
Commercial Bank (KB \v{C}R)   & 182 & 204 &  424 & 487  & \ldots \\
\v{C}EZ group    &  99 &  74 &   92 &  147  &  \ldots \\
METROSTAV  &   129 &   106 &   199  &  306  &  \ldots \\
Philip Morris \v{C}R & 5,880 & 8,090 & 11,430 & 15,730 & \ldots \\
$\ldots$ \\
\hline
\end{tabular}
\end{center}

\begin{itemize}
  \item Basic version of the portfolio management problem~\cite{Markowitz:1952:PS}
  \item Applications in various problems of risk management
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Motivation [Why?]}
\framesubtitle{How to travel efficiently?}

\begin{description}

\item[Objective] find the \emph{shortest possible} route

\item[Constraint] each city must be visited \emph{at least once}

\end{description}

\begin{center}

\begin{tabular}{lcccc}
\hline
        & Praha & Brno & Ostrava & Cheb \\
\hline
Praha   & 0~km  & 205  & 381     & 176  \\
Brno    & 207   & 0~km & 180     & 381  \\
Ostrava & 380   & 181  & 0~km    & 550  \\
Cheb    & 176   & 384  & 556     & 0~km \\
\hline
\end{tabular}
\\
\href{http://www.mapy.cz}{Distances between cities}

\end{center}

\begin{itemize}
  \item Traveling salesman problem~\cite{Cook:2014:TPS}, formulated in 1930
  \item Applications in circuit design, production planning $\ldots$
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Motivation [Why?]}
\framesubtitle{Which computer should I buy?}

%
\begin{description}

\item[Objective] select the \emph{best} notebook

\item[Criteria] prize, performance, memory and disk sizes, screen size
$\ldots$

\end{description}

\begin{center}
\begin{tabular}{lcccc}
\hline
Type        & Price & Screen Size & Disk size & Weight \\
           & [CZK] & ['']       & [GB] & [kg] \\
\hline
AMILO Pro V2035      & \bf 14,999 & 15.4 & 60  & 2.9 \\
Aspire 9814WKMi      & 65,996 & \bf 20 & 240 & 7.5 \\
Qosmio G30-194       & 93,210 &  17  & \bf 320 & 4.5 \\
Lifebook Q2010 U1400 & 105,743 & 12  & 80  & \bf 0.9 \\
\hline
\end{tabular}
\\
Parameters of selected notebooks (as of December 2006)
\end{center}

\begin{itemize}
  \item Typical multi-objective problem
  \item Used in decision-making theory~\cite{Pareto:2014:MPE}
\end{itemize}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Formalization of optimization problems}

\begin{frame}
\frametitle{Formalization of optimization problems}

\begin{description}

\item[General statement] Find $\vek{x}_{opt} \in \set{O}$,
such that
%
\begin{eqnarray*}
f( \vek{x}_{opt} ) 
\stackrel{\set{H}}{\preceq} 
f( \vek{x} ) 
& \mbox{for all} &
\vek{x} \in \set{O} 
\end{eqnarray*}

\end{description}

\begin{itemize}

\item $\vek{x}$ -- unknown \emph{variables} parametrizing the underlying
model

\item $\set{O}$ -- set of admissible solutions~(reflects \emph{constraints})

\item $f$ -- \emph{objective} function~[$f : \set{O} \rightarrow \set{H}$]

\item $\set{H}$ -- range of the objective function

\item $\stackrel{\set{H}}{\preceq}$ -- [partial] ordering
  in the range of objective function -- allows to compare two candidate
  solutions

\item $\vek{x}_{opt}$ -- \emph{optimal solution}  

\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Formalization of optimization problems}
\framesubtitle{Optimal diet problem}

\begin{itemize}

\item $N$ items on the menu

\item Unknowns -- quantities of individual items (in kg): $x_i \in \set{R}, x_i
  \geq 0$

\item Objective function
%
$$
f( \vek{x} ) = x_1 c_1 + x_2 c_2 + \cdots + x_N c_N,
$$
%
where $c_i$ is the price of the $i$-th item (per 1~kg)

\item Range of the function $\set{H} = \set{R}_0^+$

\item $\stackrel{\set{H}}{\preceq}$ corresponds to $\leq$

\item Constraints
%
$$
x_1 k_1 + x_2 k_2 + \cdots + x_N k_N \geq K,
$$
%
where $k_i$ denotes the nutrition amount of the $i$-th item and $K$
is the required nutrition value

\item $\set{O}$ corresponds to a subspace of $\set{R}^N$

\end{itemize}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Optimal 2-diet problem}
  
  \begin{description}
    \item[Objective] minimize prize
    \item[Constraints] nutrition at least $1,000$~cal
  \end{description}
  
  \begin{center}
  \small
  \begin{tabular}{lcc}
  \hline
  Item & Price & Nutrition \\
  & [CZK/1kg] & [Cal/100g] \\
  \hline
  Chicken    & 51.49  & 331\\
  Potatoes   &  8.39  &  93\\
  \hline
  \end{tabular}

\medskip 

\includegraphics[height=50mm]{figures/1_two_diet_problem}

\end{center}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Formalization of optimization problems}
\framesubtitle{Purchase of a computer}

\begin{itemize}

\item Unknown -- index of a notebook $x \in \set{N}$

\item Objective function: $M$ different criteria
%
\begin{eqnarray*}
\vek{f}( x ) = [ f_1( x ), f_2( x ), \ldots, f_M( x ) ], 
&&
\set{H} \in \set{R}^M
\end{eqnarray*}

\item Comparison of two solutions
%
\begin{eqnarray*}
\vek{f}\bigl( x^\mathrm{a} \bigr) 
\stackrel{\set{H}}{\preceq} 
\vek{f}\bigl( x^\mathrm{b} \bigr) 
& \equiv &
\left\{
\begin{array}{ccc}
f_1( x^\mathrm{a} ) & \leq & f_1( x^\mathrm{b} ) \\
f_2( x^\mathrm{a} ) & \geq & f_2( x^\mathrm{b} ) \\
f_3( x^\mathrm{a} ) & \geq & f_3( x^\mathrm{b} ) \\
f_4( x^\mathrm{a} ) & \leq & f_4( x^\mathrm{b} ) 
\end{array}
\right.
\end{eqnarray*}

\item Optimal portfolio $\rightarrow$ Next lecture

\item Traveling salesman $\rightarrow$ Lecture \#6

\end{itemize}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Rough characterization of optimization problems}

\begin{frame}
\frametitle{Rough characterization of optimization problems}

\begin{itemize}

\item \emph{Dimension} of the problem $N$ -- number of [independent] components
of $\vek{x}$ 

\item How the number of steps needed to find $\vek{x}_{opt}$ growths
with $N$?

\begin{itemize}

\item $\approx C N^k$ -- \emph{Polynomial} problem~(P)\\~
[diet, portfolio]

\item $\approx 2^N$ -- \emph{Exponential} problem~(E)

\item \emph{Non-deterministic polynomial} problem~(NP) -- problem where decision
can be made in a polynomial time by a non-deterministic oracle~[traveling
salesman]

\end{itemize}

$$
P \stackrel{?}{=} NP \prec E
$$

\item ``?'' amounts to 1 mil. USD

\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Practical characterization of optimization problems}

\begin{frame}
\frametitle{Practical characterization of optimization problems}

\centering
\includegraphics[width=1.05\textwidth]{figures/1_optimization_tree}\\
%
\bigskip
%
Example of ``optimization
tree''\\\url{https://neos-guide.org/content/optimization-taxonomy}

\end{frame}

\section{What can be solved efficiently?}

\begin{frame}
\frametitle{What can be solved efficiently?}

\begin{itemize}

\item Convex set
%
\begin{center}
\begin{tabular}{c@{\hspace{10mm}}c}
\includegraphics[height=15mm]{figures/1_convex_set} &
\includegraphics[height=15mm]{figures/1_non_convex_set}
\end{tabular}
\end{center}

\item~[Strictly] convex function
%
\begin{center}
\hspace*{-5mm}
\begin{tabular}{ccc}
\includegraphics[height=13.5mm]{figures/1_convex_function} &
\includegraphics[height=13.5mm]{figures/1_strictly_convex_function} &
\includegraphics[height=13.5mm]{figures/1_non_convex_function}
\end{tabular}
\end{center}

$$
\set{O} \mbox{ convex} + f \mbox{ convex} \approx P
$$

\item Strictly convex function on a convex set possesses the \emph{unique optimum}

\item Efficient methods of \emph{mathematical programming}~\cite{Boyd:2004:CO}

\end{itemize}
\end{frame}


\section{Summary}
%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Summary}


\begin{itemize}

\item Optimization problem $\equiv$

\begin{itemize}

\item~[Mathematical] model of the problem 

\item Optimization variables $\vek{x}$ 

\item Objective function $f$

\item Set of admissible solutions $\set{O}$

\item Range of objective function $\set{H}$ + partial order

\end{itemize}

\item Solution methods

\begin{itemize}

\item Convex problem $\rightarrow$ P $\rightarrow$ mathematical programming \\
$\rightarrow$ Lectures \#2--\#4

\item Non-convex problems $\rightarrow$ NP/E $\rightarrow$ heuristic approaches
\\ $\rightarrow$ Lectures $5+$

\end{itemize}

\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Bibliography}

\bibliography{prednaska}
\bibliographystyle{amsplain}

\end{frame}

\end{document}