function newton 
    function val = f(x)
       val = (x(1,:)-2).^2 + (x(1,:)-2*x(2,:).^2).^2;
    end

    function val = g(x)
        val = zeros(2,1);
        val(1) = -4*x(2)^2 + 4*x(1) - 4;
        val(2) = 8*x(2)*(2*x(2)^2-x(1));
    end

    function val = H(x)
        val = zeros(2,2);
        val(1,1) = 4;
        val(1,2) = -8*x(2); 
        val(2,1) = val(1,2);
        val(2,2) = 8*(6*x(2)^2-x(1));
    end
