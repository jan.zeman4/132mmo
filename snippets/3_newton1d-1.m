function newton1d
    function val = f(x) 
        val = (x.^2-1).^2+x;
    end

    function val = g(x) 
        val = 4*(x^3-x)+1;
    end

    function val = h(x) 
        val = 4*(3*x^2-1);
    end

tolerance = 1e-6;
xk = -2;
