k = 0;
gk = g(xk);

while(norm(gk) > tolerance)
    disp(['iteration = ' num2str(k) ' || gradient || = ' ... 
        num2str(norm(gk)) ' f = ' num2str(f(xk))]);
    plot(xk(1),xk(2),'or');
    pause;
