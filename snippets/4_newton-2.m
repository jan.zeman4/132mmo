clf;
h = ezcontour(@(x,y)f([x';y']), [-1,5,-3,3]);
h.LevelList = [0:0.5:10];
colorbar;
colormap gray;
hold on;
