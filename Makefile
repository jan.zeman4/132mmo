all: lecture02_handout.pdf lecture03_handout.pdf lecture04_handout.pdf 

lecture02_handout.pdf: lecture02.pdf figures/2_portfolio.pdf
	pdftk lecture02.pdf figures/2_portfolio.pdf output lecture02_handout.pdf

lecture03_handout.pdf: lecture03.pdf figures/3_newton1d_code.pdf
	pdftk lecture03.pdf figures/3_newton1d_code.pdf output lecture03_handout.pdf

lecture04_handout.pdf: lecture04.pdf figures/4_newton_code.pdf
	pdftk lecture04.pdf figures/4_newton_code.pdf output lecture04_handout.pdf